from django.urls import path

from webapp.views import index_view, add_view, detail_view

urlpatterns = [
    path('', index_view, name='index'),
    path('article/add/', add_view),
    path('article/', detail_view)

]